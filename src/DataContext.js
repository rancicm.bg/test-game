import React, {useState, createContext} from 'react';

export const DataContext = createContext();

export const DataProvider = (props) => {
  
  const [logV, setLogV] = useState({signin: true, register: false, main: false});
  const [questions, setQuestions] = useState([
    {
      emoji: '😜',
      question: 'Cesto nosi misao o osecaju zabave, uzbudjenja, smesno ili zabavno ali na drugaciji nacin od uobicajenog, saljenje na blesav nacin',
      done: true
    },
    {
      emoji: '😍',
      question: 'Cesto nosi misao o osecaju uzbudjenja iz ljubavi, zaljubljenosti, divljenja - ja sam u ljubavi sa ovom osobom ili tom stvari',
      done: false
    },
    {
      emoji:'😃',
      question: 'Cesto nosi misao o generalnom stanju srece,uzbudjenje zato sto je nesto zabavno',
      done: false
    },
    {
      emoji: '😘',
      question: 'Moze da nosi misao o poljupcu na rastanku ili za laku noc i prenosi osecanje ljubavi i privrzenosti',
      done: false
    },
    {
      emoji: '😭',
      question: 'Moze da znaci ogromnu tugu ali takodje moze i znaciti nekotrolisan smeh ili obuzimajuceg zadovoljstva',
      done: false
    },
    {
      emoji: '😤',
      question: 'Moze da znaci vise negativnih emocija kao sto su: iznerviranost, bes, ponizavajuci pogled na druge, takodje i osecaj ponosa, dominacije i uzvisenosti',
      done: false
    },
    {
      emoji: '😉',
      question: 'Moze da znaci salu, flert, skriveno znacenje, generalnu pozitivnost. Takodje poziv na igru, privrzenost, savet ili ironiju',
      done: false
    },
    {
      emoji: '🤗',
      question: 'Moze da znaci podrsku, pokazivanje ljubavi i brige, pozitivna osecanja i toplinu. Takodje moze da predstavlja jazz ruke kao uzbudjenje i entuzijazam.',
      done: false
    },
    {
      emoji: '😱',
      question: 'Nosi misao o necemu zastrasujucem i prenosi osecanja soka, straha i neverice ili intenzivnog uzbdjenja kao osoba koja voli da vrsiti',
      done: false
    },
    {
      emoji: '😬',
      question: 'Moze da predstavlja vise negativnih emocija ili napetosti, posebno nervozu, postidjenost ili cudjenje',
      done: false
    },
    {
      emoji: '😫',
      question: 'Odustajanje, frustracija, tuga ali u pozitivnom smislu moze biti divljenje i privrzenost.',
      done: false
    },
    {
      emoji: '😛',
      question: 'Osecaj da je nesto zabavno, uzbudjenje, lucidnost, simpaticnost i sreca ili samo kao sala',
      done: false
    },
    {
      emoji:'🙃',
      question: 'Cesto moze da predstavlja ironiju, sarkazam ili salu ili jednostavno ludost i blesavost',
      done: false
    },
    {
      emoji: '😴',
      question: 'Spavanje',
      done: false
    },
    {
      emoji: '😎',
      question: 'Samopouzdanje, bezbriznost ili da je nesto odlicno',
      done: false
    },
    {
      emoji: '😡',
      question: 'Mrznja, ljutnja ili bes',
      done: false
    },
    {
      emoji: '🤐',
      question: 'Prenosi misao o tajni i da ce neko cutati ali moze da znaci i da neko umukne',
      done: false
    },
    {
      emoji: '🤪',
      question: 'Moze da predstavlja blesavljenje, zabavljanje i da nosi misao o osecaju koji u zargonu znaci ludo ali u pozitivnom smislu',
      done: false
    },
    
    
  ])
  // const [regV, setRegV] = useState(false);
 
  

  return (

    <DataContext.Provider value={{
       logV : logV,
       setLogV: setLogV,
       questions: questions,
       setQuestions: setQuestions,
       }} >
      {props.children}
    </DataContext.Provider>
  )
};