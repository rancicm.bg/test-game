import React, {useState, useContext} from 'react';
import {DataContext} from './DataContext';



const Register = () => {
  
  // useEffect(() => {
  //    let path = window.location.pathname;
  //    path = '/register';
  // })
  // window.history.pushState({}, '','/register');
  // console.log(window.history.state);
  
  
  
  const [em, setEm] = useState();
  const [ps, setPs] = useState();
  const { setLogV } = useContext(DataContext);

  
  
    const email = e => {
      console.log(e.target.value);
      if(e.target.value === "") {
        alert('mora biti barem jedno slovo');
      } else {
        setEm(e.target.value)

      }
    };
    const pass = e => {
      console.log(e.target.value);
      if(e.target.value === "") {
        alert('barem jedan karakter je potreban')
      } else {
        setPs(e.target.value)

      }
    };
    const register = (e) => {
      e.preventDefault();
      console.log(em, ps);
      if(em === undefined && ps === undefined) {
        alert('mora biti barem po jedan karakter u usernamu i paswordu')
      } else {
        fetch('http://localhost:3001/register', {
          method: 'post',
          headers: {'Content-Type': 'application/json'},
          body: JSON.stringify({
            
            emailR: em,
            paswR: ps
          })
          
        }).then(response => 
          {response.json();
          if(response.status === 200) {
            setLogV({signin: true, register: false , main: false})
          };
          if(response.status === 400) {
            alert('ovaj user vec postoji');
          }
          console.log(response)
          })
        
        .catch(err => console.log(err))

      }
     
    }
      
    

    return (
      <div className="log-in">
        <form className="form-login">
          <input
            className="login-input"
            onChange={email}
            name="user-name"
            type="email"
            placeholder="Your username is your email address"
            required
            autoFocus
          ></input>
          <input
            className="login-input"
            onChange={pass}
            name="password"
            type="password"
            placeholder="pass"
            required
          ></input>
          
          <a href="/" className="register-enter" onClick={register} >Register</a>
        </form>
      </div>
    );
  };

  export default Register;