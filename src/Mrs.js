import React, {useContext} from 'react';
import SignIn from './SignIn';
import Register from './Register';
import Questions from './Questions';
import {DataContext} from './DataContext'

 const Mrs =  (props) => {
   const {logV} = useContext(DataContext);
   
  return (
    <div id="mrs" className="main">
     {logV.main === true ? <Questions /> : null}
     {logV.signin === true ? <SignIn /> : null}
     {logV.register === true ? <Register /> : null}
    </div> 
  )
}
export default Mrs;