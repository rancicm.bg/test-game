import React, {useState, useContext} from 'react';
import {DataContext} from './DataContext';

 const SighIn = () => {
   
   const [eMail, seteEmail] = useState('');
   const [pasw, setPasw] = useState('');
   const {setLogV} = useContext(DataContext);
   
  //  useEffect(() => {
  //   let path = window.location.pathname;
  //   path = '/signin'
    
  // })
      const email = e => {
        seteEmail(e.target.value);
        
      };
      const pass = e => {
        setPasw(e.target.value)
      };
      const register = (e) => {
        // e.preventDefault();
        
        setLogV( {sighin: false, register: true , main: false});
        console.log('click')
      }
      
      const sighin = (e) => {
        e.preventDefault();
        fetch('http://localhost:3001/signin', {
          method: 'post',
          headers: {'Content-Type': 'application/json'},
          body: JSON.stringify({
            
            email: eMail,
            pasw: pasw
          })
          
        }).then(response => 
          {response.json();
          if(response.status === 200) {
            setLogV({signin: false, register: false, main: true});
            // document.getElementById('mrs').className = "none";
            // document.getElementById('main').className = "main";
            
          };
          if(response.status === 400) {
            alert('nesto nije uredu, probaj ponovo ali ovaj put sa dobrim podacima');
          }
          console.log(response)
          })
        
        .catch(err => console.log(err))
      }
        console.log(`ovo je ukucan email:${eMail} i password: ${pasw}`);
        

          
  return (
     <div className="log-in">
          <form className="form-login" >
            <input
              className="login-input"
              onChange={email}
              name="user-name"
              type="text"
              placeholder="Your username is your email address"
              required
              autoFocus
            />
            <input
              className="login-input"
              onChange={pass}
              name="password"
              type="password"
              placeholder="pass"
              required
            />
            <p  className="login-register" onClick={register} >Register</p>
            <button className="login-enter" onClick={sighin} >SignIn</button>
          </form>
          
      </div> 
  )
}


export default SighIn;

